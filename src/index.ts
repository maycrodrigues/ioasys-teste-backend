import { config } from 'dotenv';
import * as http from 'http';
import mongoose from 'mongoose';
import Server from './server';

config();

const port = process.env.PORT;

Server.set('port', port);

mongoose.connect(String(process.env.MONGO_DB), { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })
  .then((_) => console.log('Conectado com o banco de dados MongoDB/Atlas'))
  .catch((err) => console.error(err.message));

http.createServer(Server).listen(port, () => console.log(`Listening: ${port}`));

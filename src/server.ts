import compression from 'compression';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import fs from 'fs';
import helmet from 'helmet';
import morgan from 'morgan';
import path from 'path';
/* import swaggerUi from 'swagger-ui-express';
import swaggerFile from '../src/swagger.json'; */


import { errorHandler, notFound } from './middlewares';

import IndexRouter from './index.router';
import UserController from './user/user.controller';
import UserRouter from './user/user.router';
import MovieRouter from './movies/movie.router';
import MovieController from './movies/movie.controller';
import RatingRouter from './rating/rating.router';
import RatingController from './rating/rating.controller';

class Server {

  public app: express.Application;

  constructor() {
    this.app = express();
    this.config();
    this.routes();
    this.middlewares();
  }

  private config() {
    const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' });
    /* this.app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerFile)); */
    this.app.use(express.urlencoded({ extended: false }));
    this.app.use(express.json({ limit: '5mb' }));
    this.app.use(cookieParser());
    this.app.use(morgan('common', { stream: accessLogStream }));
    this.app.use(compression());
    this.app.use(helmet());
    this.app.use(cors());
    this.app.use((_, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
      res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials'
      );
      res.header('Access-Control-Allow-Credentials', 'true');
      next();
    });
  }

  private routes() {
    this.app.use('/', new IndexRouter().router);
    this.app.use('/users', new UserRouter(new UserController()).router);
    this.app.use('/movies', new MovieRouter(new MovieController()).router);
    this.app.use('/ratings', new RatingRouter(new RatingController()).router);
  }

  private middlewares() {
    this.app.use(notFound);
    this.app.use(errorHandler);
  }

}

export default new Server().app;

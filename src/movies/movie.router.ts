import { Router } from 'express';
import MovieController from './movie.controller';

export default class MovieRouter {
  public router: Router;

  constructor(protected controller: MovieController) {
    this.router = Router();
    this.router.post('/', this.controller.authAsAdmin, this.controller.create);
    this.router.get('/', this.controller.authAsUser, this.controller.all);
    this.router.get('/:id', this.controller.authAsAdmin, this.controller.one);
    this.router.put('/:id', this.controller.authAsAdmin, this.controller.update);
    this.router.delete('/:id', this.controller.authAsAdmin, this.controller.delete);

    this.router.get('/filter/:type/:value', this.controller.filter);
  }
}

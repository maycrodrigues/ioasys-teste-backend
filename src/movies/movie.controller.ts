import { ROLES } from './../user/user.roles';
import { Request, Response, NextFunction } from 'express';
import { verify } from 'jsonwebtoken';
import TokenValidationUtil from '../utils/token.validation.utils';
import getType from '../utils/type.utils';
import { Controller } from '../_base/controller';
import { HttpStatusCode } from './../enums/HttpStatusCode';
import MovieRepository from './movie.repository';
import { MovieDAO } from './movie.schema';
import RatingRepository from '../rating/rating.repository';
interface IRequestFilter {
  type: string;
  value: string;
}
export default class MovieController extends Controller<MovieRepository> {
  constructor() {
    super(new MovieRepository());
  }

  public one = async (req: Request, res: Response): Promise<void> => {
    try {
      const { id } = req.params;
      const data = await this.repository.one(id);

      const rating = new RatingRepository().filter({ movie: id });

      res.status(HttpStatusCode.Ok).json({ rating, data });
    } catch (error) {
      res.status(HttpStatusCode.InternalServerError).json({ message: error.message });
    }
  };

  public filter = async(req: Request, res: Response): Promise<void> => {

    const { type, value } = getType<IRequestFilter>(req.params);

    const movies = await this.repository.filter<MovieDAO[]>({
      [type]: {
        $elemMatch: {
          $regex: String(value).toLowerCase(),
          $options: 'i'
        }
      }
    });

    if(!movies.length) {
      res.status(HttpStatusCode.BadRequest).json({ message: `Nenhum filme encontrado com o filtro: ${type} / ${value}` });

      return;
    }

    res.status(HttpStatusCode.Ok).json(movies);
  }
  
  public authAsAdmin = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    this.authenticate(req, res, next, true);
  }

  public authAsUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    this.authenticate(req, res, next, false);
  }

  private authenticate = async(req: Request, res: Response, next: NextFunction, isAdmin = true) => {
    try {
      const { success, token } = await TokenValidationUtil.isValidAuthorization(req);

      if(!success) {
        res.status(HttpStatusCode.Unauthorized).json({ auth: false, message: 'Acesso proibido' });

        return;
      }

      if(success) {
        verify(token, String(process.env.SECRET_KEY), (error: any, data: any) => {
          if (error) {
            res.status(HttpStatusCode.Unauthorized).json({ auth: false, message: 'Token inválido' });
          } else {
            const { role } = data;

            if( isAdmin && role === ROLES.ADMIN ) {
              next();

              return;
            }

            if( !isAdmin ) {
              next();
            } else {
              res.status(HttpStatusCode.Unauthorized).json({ auth: false, message: 'Usuário sem permissão.' });
            }
          }
        });
      }

    } catch (error) {
      res.status(HttpStatusCode.InternalServerError).json({ message: error.message });
    }
  }

}

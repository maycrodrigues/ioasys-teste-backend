import { Repository } from '../_base/repository';
import { MovieSchema } from './movie.schema';

export default class MovieRepository extends Repository {
  constructor() {
    super(MovieSchema);
  }
}

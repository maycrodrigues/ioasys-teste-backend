import * as mongoose from 'mongoose';
import { BaseData } from '../_base/dto';
import { baseSchema } from '../_base/schema';

const schema = new mongoose.Schema({
  ...baseSchema,
  title: { type: String, required: true },
  slug: { type: String, required: true },
  description: { type: String, required: true },
  director: { type: String, required: true },
  genre: [ String ],
  actors: [ String ],
  rating: { type: Number, default: 0 },
  cover: { type: String }
}, { timestamps: true });

interface MovieDAO extends BaseData {
  name?: string;
  mail?: string;
  password?: string;
}

const MovieSchema = mongoose.model('MovieSchema', schema);

export { MovieSchema, MovieDAO };

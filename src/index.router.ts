import { Request, Response, Router } from 'express';
import { description, version } from '../package.json';
import { HttpStatusCode } from './enums/HttpStatusCode';

export default class IndexRouter {
  public router: Router;

  constructor() {
    this.router = Router();
    this.router.get('/', (_: Request, res: Response) => {
      res.status(HttpStatusCode.Ok).json({ version, description });
    });
  }

}

import { ROLES } from './user.roles';
import * as mongoose from 'mongoose';
import { BaseData } from '../_base/dto';
import { baseSchema } from '../_base/schema';

const schema = new mongoose.Schema({
  ...baseSchema,
  name: { type: String, required: true },
  mail: { type: String, required: true },
  password: { type: String, required: true },
  role: { type: String, default: ROLES.USER }
}, { timestamps: true });

interface UserDAO extends BaseData {
  name: string;
  mail: string;
  role: string;
  password?: string;
}

const UserSchema = mongoose.model('UserSchema', schema);

export { UserSchema, UserDAO };

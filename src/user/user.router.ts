import { Router } from 'express';
import UserController from './user.controller';

export default class UserRouter {
  public router: Router;

  constructor(protected controller: UserController) {
    this.router = Router();
    this.router.get('/', this.controller.all);
    this.router.get('/:id', this.controller.authorize, this.controller.findByID);
    this.router.put('/:id', this.controller.authorize, this.controller.update);
    this.router.delete('/:id', this.controller.authorize, this.controller.delete);
    this.router.post('/register', this.controller.register);
    this.router.post('/authenticate', this.controller.authenticate);
  }
}

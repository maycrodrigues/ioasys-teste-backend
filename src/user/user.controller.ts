import { compare, hashSync } from 'bcryptjs';
import { config } from 'dotenv';
import { NextFunction, Request, Response } from 'express';
import { sign, verify } from 'jsonwebtoken';
import TokenValidationUtil from '../utils/token.validation.utils';
import getType from '../utils/type.utils';
import { Controller } from '../_base/controller';
import { HttpStatusCode } from './../enums/HttpStatusCode';
import { UserFilterFind } from './user.filter';
import UserRepository from './user.repository';
import { UserDAO } from './user.schema';

config();

export default class UserController extends Controller<UserRepository> {

  constructor() {
    super(new UserRepository());
  }

  public findByID = async (req: Request, res: Response): Promise<void> => {
    const { id } = getType<{ id: string }>(req.params);
    await this.find(req, res, { _id: id });
  };

  public update = async (req: Request, res: Response): Promise<void> => {
    try {
      const { params: { id }, body } = req;
      const { name, mail, password } = body;

      const param: any = {};
      
      if(name) { param.name = name; }
      if(mail) { param.mail = name; }
      if(password) { param.password = hashSync(password); }

      await this.repository.update(id, param);

      return this.one(req, res);
    } catch (error) {
      res.status(HttpStatusCode.InternalServerError).json({ message: error.message });
    }
  };

  public register = async (req: Request, res: Response): Promise<void> => {
    try {
      const { name, mail, password, role } = getType<{name: string, mail: string, password: string, role: string}>(req.body);

      if(await this.repository.oneBy({ mail })) {
        res.status(HttpStatusCode.BadRequest).json({ message: 'Usuário já cadastrado no sistema!' });
        
        return;
      }
      
      const user = await this.repository.create({ name, mail, password: hashSync(password), role });

      res.status(HttpStatusCode.Ok).json({ user });
    } catch (error) {
      res.status(HttpStatusCode.InternalServerError).json({ message: error.message });
    }
  }

  public authenticate = async (req: Request, res: Response): Promise<void> => {
    try {
      const { mail, password } = getType<{ mail: string, password: string }>(req.body);

      const user = await this.repository.oneBy<UserDAO>({ mail });
      if(!user) {
        res.status(HttpStatusCode.BadRequest).json({ message: 'Usuário não encontrado!' });

        return;
      }

      const { password: userPass, _id, role } = user;

      if(!await compare(password, String(userPass))) {
        res.status(HttpStatusCode.BadRequest).json({ message: 'Senha incorreta!' });

        return;
      }

      const token = sign({ _id, role }, String(process.env.SECRET_KEY), { expiresIn: String(process.env.EXPIRES_IN) });

      res.status(HttpStatusCode.Ok).json({ auth: true, token });
    } catch (error) {
      res.status(HttpStatusCode.InternalServerError).json({ message: error.message });
    }
  }
  
  public authorize = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { success, token } = await TokenValidationUtil.isValidAuthorization(req);
      if(success) {
        verify(token, String(process.env.SECRET_KEY), (error: any) => {
          if (error) {
            res.status(HttpStatusCode.Unauthorized).json({ auth: false, message: 'Token inválido' });
          } else {
            next();
          }
        });
      } else {
        res.status(HttpStatusCode.InternalServerError).json({ message: 'Token inválido' });
      }
    } catch (error) {
      res.status(HttpStatusCode.InternalServerError).json({ message: error.message });
    }
  }

  private async find(_: Request, res: Response, props = {}) {
    try {
      const user = await this.repository.oneBy<UserDAO>(props, UserFilterFind);
      res.status(HttpStatusCode.Ok).json({ user });
    } catch (error) {
      res.status(HttpStatusCode.InternalServerError).json({ message: error.message });
    }
  }
  
}

const UserFilter = '_id name mail';
const UserFilterOne = '_id uid name mail role password createdAt';
const UserFilterFind = '-_id uid name mail role createdAt updatedAt';

export { UserFilter, UserFilterOne, UserFilterFind };

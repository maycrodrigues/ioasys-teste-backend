import { Repository } from './../_base/repository';
import { UserFilter, UserFilterOne } from './user.filter';
import { UserSchema } from './user.schema';

export default class UserRepository extends Repository {
  constructor() {
    super(UserSchema);
  }

  public all = async (page = 1, max = 9, active = true) => {
    return this.model
      .find({ active }, UserFilter)
      .skip(max * page - max)
      .limit(max)
      .sort({ createdAt: -1 });
  };

  public async oneBy<T>(props: any, filter = UserFilterOne):Promise<T> {
    const data = this.model.findOne({...props, active: true}, filter);

    return data as unknown as T;
  }

}

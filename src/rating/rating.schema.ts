import * as mongoose from 'mongoose';
import { baseSchema } from '../_base/schema';

const schema = new mongoose.Schema({
  ...baseSchema,
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'UserSchema' },
  movie: { type: mongoose.Schema.Types.ObjectId, ref: 'MovieSchema' },
  rating: { type: Number },
}, { timestamps: true });

const RatingSchema = mongoose.model('RatingSchema', schema);

export { RatingSchema };

import { Request, Response } from 'express';
import { HttpStatusCode } from '../enums/HttpStatusCode';
import MovieRepository from '../movies/movie.repository';
import UserRepository from '../user/user.repository';
import getType from '../utils/type.utils';
import { Controller } from '../_base/controller';
import RatingRepository from './rating.repository';

export default class RatingController extends Controller<RatingRepository> {
  constructor() {
    super(new RatingRepository());
  }

  public rate = async (req: Request, res: Response) => {
    try {
      const { user, movie, rating } = getType<{user: string, movie: string, rating: number}>(req.body);
      
      const ratingCreated = await this.repository.create({ user, movie, rating });

      const userRepository = new UserRepository();
      const userData = await userRepository.one(user);

      const movieRepository = new MovieRepository();
      const movieData = await movieRepository.one(movie);

      // Rating
      // User
      // Movie

      res.status(HttpStatusCode.Ok).json({ userData, movieData, ratingCreated });
    } catch (error) {
      res.status(HttpStatusCode.InternalServerError).json({ message: error.message });
    }

  }
}

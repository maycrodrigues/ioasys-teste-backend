import { Router } from 'express';
import RatingController from './rating.controller';

export default class RatingRouter {
  public router: Router;

  constructor(protected controller: RatingController) {
    this.router = Router();
    this.router.post('/', this.controller.rate);
    this.router.get('/', this.controller.all);
    this.router.get('/:id', this.controller.one);
    this.router.put('/:id', this.controller.update);
    this.router.delete('/:id', this.controller.delete);
  }
}

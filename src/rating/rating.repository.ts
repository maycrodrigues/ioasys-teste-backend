import { Repository } from '../_base/repository';
import { RatingSchema } from './rating.schema';

export default class RatingRepository extends Repository {
  constructor() {
    super(RatingSchema);
  }

  public async filter<T>(props: any):Promise<T> {
    const filtered = {...props, active: true};
    console.log(filtered);
    const data = this.model.find({...props, active: true});
    return data as unknown as T;
  }

}

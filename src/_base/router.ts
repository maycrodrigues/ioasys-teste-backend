import { Router } from 'express';
import { Controller } from './controller';
import { Repository } from './repository';

export default class BaseRouter {
  public router: Router;

  constructor(protected controller: Controller<Repository>) {
    this.router = Router();
    this.router.get('/', this.controller.all);
    this.router.post('/', this.controller.create);
    this.router.get('/:id', this.controller.one);
    this.router.put('/:id', this.controller.update);
    this.router.delete('/:id', this.controller.delete);
  }
}

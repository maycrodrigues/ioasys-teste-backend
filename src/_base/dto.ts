export interface BaseData {
  _id: string;
  uid?: string;
  active?: boolean;
}

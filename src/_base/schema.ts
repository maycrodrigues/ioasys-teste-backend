import shortid from 'shortid';

export const baseSchema = {
  uid: {
    type: String,
    default: shortid.generate,
    trim: true,
    index: true,
    unique: true
  },
  active: {
    type: Boolean,
    default: true
  }
};

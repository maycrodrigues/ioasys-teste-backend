import { Request, Response } from 'express';
import { HttpStatusCode } from '../enums/HttpStatusCode';
import { Repository } from './repository';

export abstract class Controller<T extends Repository> {
  constructor(protected repository: T) { }

  public create = async (req: Request, res: Response): Promise<void> => {
    try {
      const data = await this.repository.create(req.body);

      res.status(HttpStatusCode.Ok).json({ data });
    } catch (error) {
      res.status(HttpStatusCode.InternalServerError).json({ message: error.message });
    }
  };

  public all = async (_req: Request, res: Response): Promise<void> => {
    try {
      const data = await this.repository.all();
      const total = await this.repository.count();

      res.status(HttpStatusCode.Ok).json({ data, total });
    } catch (error) {
      res.status(HttpStatusCode.InternalServerError).json({ message: error.message });
    }
  };

  public one = async (req: Request, res: Response): Promise<void> => {
    try {
      const { id } = req.params;
      const data = await this.repository.one(id);

      res.status(HttpStatusCode.Ok).json({ data });
    } catch (error) {
      res.status(HttpStatusCode.InternalServerError).json({ message: error.message });
    }
  };

  public update = async (req: Request, res: Response): Promise<void> => {
    try {
      const { params, body } = req;
      const { id } = params;

      await this.repository.update(id, body);

      return this.one(req, res);
    } catch (error) {
      res.status(HttpStatusCode.InternalServerError).json({ message: error.message });
    }
  };

  public delete = async (req: Request, res: Response): Promise<void> => {
    try {
      const { id } = req.params;

      await this.repository.delete(id);

      res.status(HttpStatusCode.Ok).json({ Ok: true });
    } catch (error) {
      res.status(HttpStatusCode.InternalServerError).json({ message: error.message });
    }
  };

}

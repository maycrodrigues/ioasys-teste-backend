import { Document, Model } from 'mongoose';

export class Repository {
  constructor(protected model: Model<Document>) { }

  public all = async () => this.model.find();
  public create = async (entity: any) => this.model.create(entity);
  public update = async (id: string, entity: any) => this.model.updateOne({ _id: id }, { ...entity, updatedAt: new Date().toISOString });
  public delete = async (id: string) => this.update(id, { active: false });
  public one = async (id: string, aditionalProps = {}) => this.model.findOne({ _id: id, active: true, ...aditionalProps });
  public count = async () => this.model.countDocuments();
  
  public async filter<T>(props: any):Promise<T> {
    const filtered = {...props, active: true};
    console.log(filtered);
    const data = this.model.find({...props, active: true});
    return data as unknown as T;
  }
}

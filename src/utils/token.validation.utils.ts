import { Request } from 'express';

interface IValidToken {
  token: string;
  success: boolean;
}

export default class TokenValidationUtil {

  public static isValidAuthorization = async (req: Request): Promise<IValidToken> => {
    const authCode = String(req.headers['authorization']);
    if (!authCode) {
      return { token:'', success: false };
    }

    const [_, token] = authCode.split(' ');
    
    return { token, success: !!token };
  }

}
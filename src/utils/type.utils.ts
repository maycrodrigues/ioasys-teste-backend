export default function getType<T>(param: any): T {
  return param as unknown as T;
}

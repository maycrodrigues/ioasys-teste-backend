import { ErrorRequestHandler, NextFunction, Request, Response } from 'express';
import { HttpStatusCode } from './enums/HttpStatusCode';

const notFound = (request: Request, response: Response, next: NextFunction) => {
  response.status(HttpStatusCode.NotFound);
  const error = new Error(`Not Found - ${request.originalUrl}`);
  next(error);
}

const errorHandler = (error: ErrorRequestHandler, _request: Request, response: Response) => {
  const statusCode = response.statusCode !== HttpStatusCode.Ok ? response.statusCode : HttpStatusCode.InternalServerError;
  response.status(statusCode);
  response.json( process.env.NODE_ENV !== 'prod' ? error : { error: 'Internal server error' });
}

export { notFound, errorHandler };

module.exports = {
  verbose: true,
  testEnvironment: 'node',
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  },
  testRegex: '((\\.|/)(test|spec))\\.(js?|ts?)$',
  testPathIgnorePatterns: ['/node_modules/', '/integration/'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  notify: true,
  notifyMode: 'change'
}
